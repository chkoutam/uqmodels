"""Docstring à completer"""
from sklearn.covariance import EmpiricalCovariance
import numpy as np
import scipy

# Threesold value based on the x% confident interval for a standards normal law.
def fonction1(alpha, sigma=1):
    """Descriptions plus détaillées sur la fonction/méthode. Vous pouvez
    expliquer ici comment elle fonctionne et à quoi elle sert.


    :param Returns: param alpha:
    :param sigma: Default value = 1)
    :param alpha: 

    """
    return scipy.stats.norm.ppf(alpha / 2, 0, sigma)


def fonction2(y_pred, sigma, alpha, mode="sigma"):
    """Compute y_upper and y_lower boundary from gaussian hypothesis (sigma or 2sigma)

    :param y_pred: param sigma:
    :param alpha: param mode:  (Default value = "sigma")
    :param sigma: 
    :param mode:  (Default value = "sigma")

    """

    return y_lower, y_upper


# Thresholding of extrem values
def score_seuil(s, per_seuil=0.995, local=True):
    """"Docstring à completer

    :param s: param per_seuil:  
    :param local: 
    :param per_seuil:  

    """
   

    return 5

# Cut of extrem variance outside of quantiles [pmin,pmax]
def fonction3(var, cut_min, cut_max):
    """"Docstring à completer

    :param var: param cut_min:
    :param cut_max: 
    :param cut_min: 

    """

    return var

# Cut of extrem variance outside of quantiles [pmin,pmax]
def fonction4(var, cut_min, cut_max):
    """"Docstring à completer

    :param var: param cut_min:
    :param cut_max: 
    :param cut_min: 

    """

    return var


# Cut of extrem variance outside of quantiles [pmin,pmax]
def fonction5(var, cut_min, cut_max):
    """"Docstring à completer

    :param var: param cut_min:
    :param cut_max: 
    :param cut_min: 

    """

    return var

