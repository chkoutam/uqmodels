muqs package
============

Submodules
----------

muqs.anomaly\_score module
--------------------------

.. automodule:: muqs.anomaly_score
   :members:
   :undoc-members:
   :show-inheritance:

muqs.metrics module
-------------------

.. automodule:: muqs.metrics
   :members:
   :undoc-members:
   :show-inheritance:

muqs.predictor module
---------------------

.. automodule:: muqs.predictor
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: muqs
   :members:
   :undoc-members:
   :show-inheritance:
