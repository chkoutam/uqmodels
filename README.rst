Modèle Universel de Qualité (MUQ) 🌐
===================================

Premiers pas 🚀
--------------

Voici une liste de recommandations pour bien commencer avec GitLab.

Si vous êtes déjà un expert, modifiez simplement ce README.md à votre
guise. Vous voulez faire simple? `Utilisez le modèle en bas de
page <#editing-this-readme>`__!

Ajoutez vos fichiers 📁
----------------------

-  ☐
   `Créez <https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file>`__
   ou
   `uploadez <https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file>`__
   des fichiers 📤
-  ☐ `Ajoutez des fichiers à l’aide de la ligne de
   commande <https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line>`__
   ou poussez un dépôt Git existant avec la commande suivante 🖥️ :

\``\` cd existing_repo git remote add origin https://gitlab.com//muq.git
git branch -M main git push -uf origin main \``\`

Intégrez vos outils 🛠️
----------------------

-  ☐ `Configurez les intégrations du
   projet <https://gitlab.com/%3Cyour_username%3E/muq/-/settings/integrations>`__

Collaborez avec votre équipe 👥
------------------------------

-  ☐ `Invitez des membres de l’équipe et des
   collaborateurs <https://docs.gitlab.com/ee/user/project/members/>`__
-  ☐ `Créez une nouvelle demande de
   fusion <https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html>`__
-  ☐ `Fermez automatiquement les problèmes à partir des demandes de
   fusion <https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically>`__
-  ☐ `Activez les approbations de demandes de
   fusion <https://docs.gitlab.com/ee/user/project/merge_requests/approvals/>`__
-  ☐ `Fusionnez automatiquement lorsque le pipeline
   réussit <https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html>`__

Testez et déployez 🚦
--------------------

Utilisez l’intégration continue intégrée à GitLab.

-  ☐ `Commencez avec GitLab
   CI/CD <https://docs.gitlab.com/ee/ci/quick_start/index.html>`__
-  ☐ `Analysez votre code pour les vulnérabilités connues avec le test
   de sécurité d’application statique
   (SAST) <https://docs.gitlab.com/ee/user/application_security/sast/>`__
-  ☐ `Déployez sur Kubernetes, Amazon EC2, ou Amazon ECS en utilisant
   Auto
   Deploy <https://docs.gitlab.com/ee/topics/autodevops/requirements.html>`__
-  ☐ `Utilisez des déploiements basés sur pull pour une meilleure
   gestion de
   Kubernetes <https://docs.gitlab.com/ee/user/clusters/agent/>`__
-  ☐ `Configurez des environnements
   protégés <https://docs.gitlab.com/ee/ci/environments/protected_environments.html>`__

--------------

Code de test aléatoire 🎲
========================

\```python import unittest

class TestMethods(unittest.TestCase):

::

   def test_add(self):
       self.assertEqual(add(1, 2), 3)

   def test_subtract(self):
       self.assertEqual
