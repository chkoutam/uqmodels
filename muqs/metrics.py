"""
Metrics module for UQ method evaluation.
"""
import numpy as np
from sklearn.metrics import mean_absolute_error, mean_squared_error

# @TODO Add meta class for automatic metric evaluation on the benchmark


def mae(y_true, y_pred):
    return mean_absolute_error(y_pred, y_true)


def rmse(y_true, y_pred):
    return np.sqrt(mean_squared_error(y_pred, y_true))


def perf_pred(y_pred, y):
    return mean_absolute_error(y_pred, y), rmse(y_pred, y)


