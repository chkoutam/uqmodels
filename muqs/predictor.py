"""
This module provides wrappings for ML models.
"""

from abc import ABC, abstractmethod
import numpy as np
import scipy




class Base(ABC):
    """Abstract structure of a base predictor class."""

    def __init__(self):
        self.is_trained = False

    def _format(self, X: np.array, y: np.array):
        """Format data to be consistent with the the fit and predict methods.

        :param X: features
        :param X: np.array: 
        :param y: np.array: 
        :returns: formated_X, formated_y

        """
        return X, y

    @abstractmethod
    def fit(self, X: np.array, y: np.array, **kwargs) -> None:
        """Fit model to the training data.

        :param X: train features
        :param y: train labels
        :param X: np.array: 
        :param y: np.array: 
        :param **kwargs: 

        """
        pass

    @abstractmethod
    def predict(self, X: np.array, **kwargs):
        """Compute predictions on new examples.
        
        :param X: np.array: 
        :param **kwargs: 
        :returns: y_pred, y_lower, y_upper, sigma_pred

        """
        pass

    def _tuning(self, X: np.array, y: np.array, **kwargs):
        """Fine-tune the model's hyperparameters.

        :param X: features from the validation dataset
        :param y: labels from the validation dataset
        :param X: np.array: 
        :param y: np.array: 
        :param **kwargs: 

        """
        pass


